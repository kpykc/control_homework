= Single-link manipulator control
:author: Oleksandr Lavrushchenko
:Author Initials: OL
:email:     aleksandr.lavrushchenko@gmail.com
:toc:
:icons:
:doctype: book
:encoding: utf-8
:revnumber: v0.3
:revdate: 2011-01-11
:localdate: {sys: date +%Y-%m-%d}
:description: single-link manipulator model
:lang: en

[[part1]]
== Single-link manipulator model 

=== Model of single-link manipulator

Create a model of single-link manipulator for given data.

[[eq1]]
[latexmath]
.eq1
++++
J_m &= 3.2e-4 ~ kg \cdot m^2\\
K_b &= 0.105 ~ V \cdot s \\
K_m &= 0.105 ~ \frac{N\cdot m^2}{A}\\
L &= 9e-4 ~ H\\
R &= 0.56 ~ \Omega \\
B_m &= 5.5e-5 ~ \frac{kg \cdot m^2}{s} \\
I_{a_{min}} &= -12 ~ A \\ 
I_{a_{max}} &= 12 ~A
++++

To study system properties we build step response of system by feeding 
step input.
[[fig1]]
.System model
image::model_new.pdf[scaledwidth="90%"]



=== Simplified model of single-link manipulator

Neglect system's electrical time constant.
[[fig2]]
.Simplified model
image::model_simp.pdf[scaledwidth="90%"]

=== Simulation of proposed models

[[fig3]]
.System step response
image::model_new_sr.pdf[scaledwidth="80%"]


[[fig4]]
.Simplified system step response
image::model_simp_sr.pdf[scaledwidth="80%"]

Electrical time constant latexmath:[$L/R$] is much smaller than the 
mechanical time constant
latexmath:[$J_m/B_m$] therefore it can be neglected. As we see from the 
fig. <<fig3>>. and fig. <<fig4>> there are no significant
difference between responses of models.



== PD controller design
Design the PD controller, assuming that the both θ and 
latexmath:[$\dot{θ̇}$] 
are measured and
natural critical frequency of the arm is latexmath:[$f = 40$] Hz .

[[fig5]]
.System model with PD controller
image::model_pd.pdf[scaledwidth="90%"]



Using a PD controller, the control input latexmath:[$U(s)$] is given 
in the Laplace domain.
latexmath:[$K_p$] are latexmath:[$K_d$] the proportional and derivative 
gains.

[[eq2]]
[latexmath]
.eq2
++++
K_p &= \omega^2 \cdot J \\
K_d &= 2 \cdot \zeta \cdot \omega \cdot J - B \\
B &= B_m + K_b \cdot K_m/R \\
J &= J_m \\
\zeta &= 1 
++++

Where:

- latexmath:[$B$] -- effective damping;
- latexmath:[$\zeta=1$] -- damping ratio (the response is critically damped).

Saturation is used to limit magnitude of the input signal.

[[fig2-1]]
.System step response
image::model_pd_sr.pdf[scaledwidth="90%"]


//[[fig2-1]]
//.System step response
//image::pd_step_response.pdf[scaledwidth="90%"]
//image::model_pid_new_sr.png[]


== Trajectory control

Test the behavior of the control system for (adjust the controller parameters
if necessary).

=== Point-to-point trajectory

Point-to-point control from θ=0 to θ=3.

To implement this type of trajectory, we just need to set parameters of 
"Step" block.

[[fig3-1]]
.System step response
image::model_pd_p2p_sr.pdf[scaledwidth="90%"]

=== Cubic polynomial trajectory

Cubic polynomial trajectory from θ=0 to θ=3, latexmath:[$t_f=1$]

To create a source signal we use "Clock" and "Polynomial" block.

Polynomial parameters were found in matlab, according to the following system of
four linear equations:
[[eq3]]
[latexmath]
.eq3
++++
q_0 &= a_0 + a_1 t_0 + a_2 t_0^2 +a_3 t_0^3 \\
v_0 &= a_1 + 2 a_2 t_0 + 3 a_3 t_0^2 \\
q_f &= a_f + a_1 t_f + a_2 t_f^2 +a_3 t_f^3 \\\\
v_f &= a_1 + 2 a_2 t_f + 3 a_3 t_f^2 \\
a_0 = 0,~a_1&=0,~a_2=9,~a_3=-6
++++

[[fig3-2]]
.System model
image::model_pd_cubic.pdf[scaledwidth="80%"]

[[fig3-3]]
.System response - position
image::model_pd_cubic_sr_pos.pdf[scaledwidth="60%"]

[[fig3-4]]
.System response - velocity
image::model_pd_cubic_sr_vel.pdf[scaledwidth="60%"]

[[fig3-5]]
.System response - acceleration
image::model_pd_cubic_sr_acc.pdf[scaledwidth="60%"]

=== LSPB trajectory 

LSPB trajectory with given parameters:
θ from θ=0 to θ=3, latexmath:[$t_b= 0.2$], latexmath:[$t_f= 1$].

[[fig3-6]]
.System model
image::model_pd_lspb.pdf[scaledwidth="90%"]

[[fig3-7]]
.System response - position
image::model_pd_lspb_sr_pos.pdf[scaledwidth="60%"]

[[fig3-8]]
.System response - velocity
image::model_pd_lspb_sr_vel.pdf[scaledwidth="60%"]

[[fig3-9]]
.System response - acceleration
image::model_pd_lspb_sr_acc.pdf[scaledwidth="60%"]


=== Influence of disturbance change

Check influence of the disturbance change from latexmath:[$t_l/r= 0 ~ N\cdot m$] 
to latexmath:[$t_l/r= 0.1 ~ N\cdot m$] in latexmath:[$t_c= 0.8 ~ s$] 

As a source of the disturbance was used "Ramp" block, with given data
slope parameter will be equal to m = Δy/Δx = 0.125. System model is same as
in fig. <<fig5>> but disturbance switch enables disturbance.

[[fig3-11]]
.Disturbance function
image::model_pd_dist_fn.pdf[scaledwidth="60%"]

[[fig3-12]]
.System response
image::model_pd_dist_sr.pdf[scaledwidth="60%"]

//[[fig3-13]]
//.System response - delta
//image::pd_dist_response_delta.png[]

The error between true and desired positions is small, Δ<0.005.

=== Influence of latexmath:[$K_p$] on steady-state control error

Influence of latexmath:[$K_p$] on steady-state control error tested
by finding system responce with values of calculated latexmath:[$K_p$]
multiplied by factor of 0.5,2,4,8,16.

[[fig3-14]]
.System response latexmath:[$2 \cdot K_p$]
image::pd_step_response_2Kp.png[scaledwidth="80%"]

[[fig3-15]]
.System response latexmath:[$4 \cdot K_p$]
image::pd_step_response_4Kp.png[scaledwidth="80%"]

[[fig3-16]]
.System response latexmath:[$8 \cdot K_p$]
image::pd_step_response_8Kp.png[scaledwidth="80%"]

[[fig3-17]]
.System response latexmath:[$16 \cdot K_p$]
image::pd_step_response_16Kp.png[scaledwidth="80%"]

[[fig3-18]]
.System response latexmath:[$0.5 \cdot K_p$]
image::pd_step_response_0.5Kp.png[scaledwidth="80%"]

As can be seen, latexmath:[$K_p$] proportional to deviation from desired
value. As latexmath:[$K_p$] is proportional to error his main goal is to 
minimize error.

== PID controller design

Eliminate the steady-state control error using the PID controller and test the
behavior of the resulting control system for situations as in point 5, apply
anti-windup mechanism if necessary.

[[fig4-1]]
.System model
image::model_pid_new.pdf[scaledwidth="80%"]

[[fig4-2]]
.System step response
image::model_pid_new_sr.pdf[scaledwidth="60%"]

[[fig4-3]]
.System step response
image::pid_step_response.png[scaledwidth="60%"]

Test the behavior of the control system for (adjust the controller parameters
if necessary).

=== Point-to-point trajectory

Point-to-point control from θ=0 to θ=3.

[[fig5-1]]
.System step response
image::model_pid_p2p_sr.pdf[scaledwidth="60%"]

=== Cubic polynomial trajectory

Cubic polynomial trajectory from θ=0 to θ=3, latexmath:[$t_f=1$]

[[fig5-2]]
.System model
image::model_pid_cubic.pdf[scaledwidth="90%"]

[[fig5-3]]
.System response - position
image::model_pid_cubic_sr_pos.pdf[scaledwidth="60%"]

[[fig5-4]]
.System response - velocity
image::model_pid_cubic_sr_vel.pdf[scaledwidth="60%"]

[[fig5-5]]
.System response - acceleration
image::model_pid_cubic_sr_acc.pdf[scaledwidth="60%"]

=== LSPB trajectory 

LSPB trajectory with given parameters:
θ from θ=0 to θ=3, latexmath:[$t_b= 0.2$], latexmath:[$t_f= 1$].

[[fig5-6]]
.System model
image::model_pd_lspb.pdf[scaledwidth="90%"]

[[fig5-7]]
.System response - position
image::model_pid_lspb_sr_pos.pdf[scaledwidth="60%"]

[[fig5-8]]
.System response - velocity
image::model_pid_lspb_sr_vel.pdf[scaledwidth="60%"]

[[fig5-9]]
.System response - acceleration
image::model_pid_lspb_sr_acc.pdf[scaledwidth="60%"]


=== Influence of disturbance change

Check influence of the disturbance change from latexmath:[$t_l/r= 0 ~ N\cdot m$] 
to latexmath:[$t_l/r= 0.1 ~ N\cdot m$] in latexmath:[$t_c= 0.8 ~ s$] 

[[fig5-12]]
.System response
image::model_pid_dist_sr.pdf[scaledwidth="60%"]

//[[fig5-13]]
//.System response - delta
//image::pd_dist_response_delta.png[]

The error between true and desired positions is small, Δ<0.0005.



== System stability zone for latexmath:[$K_p$]


Find the range of values of the gain for which the control system with the
simplified model and the PID controller is stable.
Closed-loop characteristic polynomial:

[[eq4]]
[latexmath]
.eq5
++++
\Omega(s) &= J \cdot s^3 + (B+K_d)\cdot s^2 + K_p \cdot s + K_i
++++

Stability condition (from Hurwitz criterion):
[[eq6]]
[latexmath]
.eq6
++++
K_p &= \omega^2 \cdot J 
M = \begin{vmatrix}
       B+K_d & J \\[0.3em]
       K_i   & K_p 
     \end{vmatrix} \Longrightarrow (B+K_d)\cdot K_p - K_i \cdot J 
     \Longrightarrow K_p > \frac{K_i \cdot J}{B+K_d}\\
K_p &> 6.7376
++++

[[fig6-0]]
.System step response
image::model_pid_Kp_margin.pdf[scaledwidth="60%"]

== Feedforward-feedback control system

[[fig6-1]]
.System step response
image::model_ff.pdf[scaledwidth="60%"]


[[fig6-2]]
.System step response
image::model_ff_step_response.pdf[scaledwidth="60%"]


[[fig6-3]]
.System step response comparison between PID and Feedforward models
image::model_ff_sr.pdf[scaledwidth="60%"]

As we can see from fig. <<fig6-3>>, the desired position at approximately
same setting time, but PID controller has less overshoot.


//[format="csv",cols="^1,4*2",options="header"]
//|===================================================
//include::data/verilog-operators.csv[]
//|===================================================

//:language: verilog

//. Source code for excersise
//[source, matlab]
//~~~~
//include::init.m[]
//~~~~
