#SOURCES = index.jemdoc contact.jemdoc mps.jemdoc gyro.jemdoc download.jemdoc
#OBJECTS=$(SOURCES:.jemdoc=.html)


#DBLATEXOPTS= -b xelatex -p ./dblatex/asciidoc-dblatex.xsl \
#=======
DBLATEXOPTS= -b xetex -p ./asciidoc-dblatex.xsl \
-s ./asciidoc-dblatex.sty  \
--param=admon.graphics.extension=png  --param=admon.graphics=1  \
--param=admon.graphics.path=./img --param=admon.textlabel=0 \
--param=figure.note=note --param=figure.important=important \
--param=figure.warning=warning --param=figure.tip=tip \
--param=latex.class.options=14pt,a4paper,oneside,headings=onelinechapter,nochapterprefix,chapterwithoutprefix,chapterwithoutprefixline,chapterprefix=false,appendixprefix \
--param=latex.output.revhistory=0 --param=doc.toc.show=0 \
--param=doc.layout="index"

#--param=latex.class.options=14pt,a4paper,oneside \
# --param=doc.lot.show=figure,table
#-P draft.watermark=1 
#-P admon.graphics=1 -P navig.graphics=1 
#-P latex.admonition.path=./img/
#--param=latex.class.options=14pt

ADOPTS= --conf-file=./docbook45.conf  \
--conf-file=./asciidoc.conf
#\
#--conf-file=./lang-ru.conf 

# save build files option: -k
# -a lang=ru
A2XOPTS= -d book --icons  -v 

#XSLOPTS= -stringparam admon.graphics 1  
#-stringparam admon.textlabel  0 -stringparam admon.graphics.extension pdf admon.graphics.path and admon.graphics.extension 

A2X= a2x ${A2XOPTS} --asciidoc-opts="${ADOPTS}" --dblatex-opts "${DBLATEXOPTS}"  
#--xsltproc-opts="${XSLOPTS}"

#--conf-file
#-p ./dblatex/asciidoc-dblatex.xsl \
#-s ./dblatex/asciidoc-dblatex.sty

all: pdf
	@echo "Posible actions:"
	@echo "pdf"
#	@echo "test"
#	@echo "upload"



pdf: texc
#	${A2X} -f pdf report.txt

pdfa:
	${A2X} -f pdf report.txt	
	
pdfcrop:	
	find ./ -name model\*.pdf -exec pdfcrop '{}' '{}' \;

texk:
	${A2X} -k -f tex report.txt
	
texc: texk
	xelatex -output-driver="xdvipdfmx -V 5" report.tex

clean:
	rm -rf *.{aux,log,out,cb,idx,glo}
	rm -rf *.html
#	rm *~

