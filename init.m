%% Reinitialize the environment
close all; clear; clc;

%% Save model diagrams as images

models = {

    'model_new';
    'model_simp';
    'model_pd';
    'model_pd_step';
    'model_pd_p2p';
    'model_pd_cubic';
    'model_pd_lspb';
    'model_pid_new';
    'model_pid_p2p'; 
    'model_pid_cubic'; 
    'model_pid_lspb'; 
    'model_ff'
    };


for i=1:size(models,1)
    model_name = models(i);
    model_fn = strcat(model_name,'.pdf');
    open_system(model_name);
    hndl = get_param(model_name,'Handle');
    saveas(hndl{1}, model_fn{1});
    %print('-smodel_simp','Figure2.png') % works too
    bdclose(model_name);
end

%% model simulator parameters
paramNameValStruct.SimulationMode = 'normal';
paramNameValStruct.AbsTol         = '1e-5';
paramNameValStruct.SaveState      = 'on';
paramNameValStruct.StateSaveName  = 'xoutNew';
paramNameValStruct.SaveOutput     = 'on';
paramNameValStruct.OutputSaveName = 'youtNew';

%% Input data
f =40; %Hz
Jm = 3.2e-4; %kg*m^2
zeta = 1;
Kb = 0.105; %V*s
Km = 0.105; %N*m/A
Bm = 5.5e-5; %kg*m^2/s
R = 0.56; %Om
minIa = -12; %A
maxIa = 12; %A
omega = 2*pi*f;

Umin = minIa/R;
Umax = maxIa/R;
answer = sprintf('Umin = %f, Umax = %f', Umin, Umax);
disp(answer);
%pause;

% J, B calculation

J = Jm;
B = Bm + (Kb*Km)/R;

%% System model

simOut = sim('model_new.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
%plot(t.Data,u.Data,t.Data,y.Data);
plot(t.Data,y.Data);
grid;
xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_new_sr.pdf');

%% System model simplified
simOut = sim('model_simp.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
%plot(t.Data,u.Data,t.Data,y.Data);
plot(t.Data,y.Data);
grid;
xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_simp_sr.pdf');


%% PD controller coefficients

Kp = (omega^2) * J;
Kd = 2*zeta*omega*J - B;

disp('PD parameters:');
answer = sprintf('Kp = %f, Kd = %f', Kp, Kd);
disp(answer);
%pause;

%% PD controller simulation
simOut = sim('model_pd.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
%plot(t.Data,u.Data,t.Data,y.Data);
plot(t.Data,y.Data);
grid;
%xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_pd_sr.pdf');

%% P2P simulation

simOut = sim('model_pd_p2p.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
%plot(t.Data,u.Data,t.Data,y.Data);
plot(t.Data,y.Data);
grid;
xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_pd_p2p_sr.pdf');

%% Polynomial trajectory coefficients

A=[1 0 0 0
   0 1 0 0
   1 1 1 1
   0 1 2 3];

C=[0;0;3;0];

% solve
A=inv(A);
x=A*C;

disp('Polynomial parameters:');
disp(x);
%pause;

%% PD cubic trajectory simulation

simOut = sim('model_pd_cubic.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
dy = simOut.find('dy'); % system output vector
ddy = simOut.find('ddy'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
saveas(f, 'model_pd_cubic_sr_pos.pdf');

plot(t.Data,dy.Data);grid;
saveas(f, 'model_pd_cubic_sr_vel.pdf');

plot(t.Data,ddy.Data);grid;
saveas(f, 'model_pd_cubic_sr_acc.pdf');

%% PD LSPB simulation

simOut = sim('model_pd_lspb.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
dy = simOut.find('dy'); % system output vector
ddy = simOut.find('ddy'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
saveas(f, 'model_pd_lspb_sr_pos.pdf');

plot(t.Data,dy.Data);grid;
saveas(f, 'model_pd_lspb_sr_vel.pdf');

plot(t.Data,ddy.Data);grid;
saveas(f, 'model_pd_lspb_sr_acc.pdf');

%% PD disturbance

simOut = sim('model_pd_dist.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
d = simOut.find('d'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');

plot(t.Data,d.Data);grid;
saveas(f, 'model_pd_dist_fn.pdf');

plot(t.Data,y.Data);grid;
saveas(f, 'model_pd_dist_sr.pdf');


%% K_p influence

simOut = sim('model_pd.mdl',paramNameValStruct);
u = simOut.find('u');
y = simOut.find('y');
x = simOut.find('x');
t = simOut.find('t');
plot(t.Data,u.Data,t.Data,y.Data);
% [A,B,C,D]=linmod('model_pd_step')
%[numcl,dencl]=ss2tf(A,B,C,D)
%step(numcl,dencl)
%step(numcl,dencl, 1)

%% PID controller coefficients
Kp = 3*J*omega^2;
Kd = 3*J*omega - B;
Ki = J*omega^3;


disp('PID parameters:');
clear answer
disp('Kp=');
disp(Kp);
disp('Kd=');
disp(Kd);
disp('Ki=');
disp(Ki);


%answer = sprintf('Kp = %f, Kd = %f, Ki = %f', Kp, Kd, Ki);
%disp(answer);
%pause;

%% PID P2P simulation

simOut = sim('model_pid_p2p.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
%xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_pid_p2p_sr.pdf');


%[A,B,C,D]=linmod('model_pd_step');
%[numcl,dencl]=ss2tf(A,B,C,D);
%step(numcl,dencl, 1);
%step(numcl,dencl);


%% PID cubic trajectory simulation

simOut = sim('model_pid_cubic.mdl',paramNameValStruct);

u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
dy = simOut.find('dy'); % system output vector
ddy = simOut.find('ddy'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
saveas(f, 'model_pid_cubic_sr_pos.pdf');

plot(t.Data,dy.Data);grid;
saveas(f, 'model_pid_cubic_sr_vel.pdf');

plot(t.Data,ddy.Data);grid;
saveas(f, 'model_pid_cubic_sr_acc.pdf');

%% PID LSPB simulation

simOut = sim('model_pid_lspb.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
dy = simOut.find('dy'); % system output vector
ddy = simOut.find('ddy'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
saveas(f, 'model_pid_lspb_sr_pos.pdf');

plot(t.Data,dy.Data);grid;
saveas(f, 'model_pid_lspb_sr_vel.pdf');

plot(t.Data,ddy.Data);grid;
saveas(f, 'model_pid_lspb_sr_acc.pdf');

%% PID disturbance simulation
simOut = sim('model_pid_dist.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;
%xlabel('Time'); ylabel('Position'); title('System response'); legend('System trajectory');

saveas(f, 'model_pid_dist_sr.pdf');

%% PID disturbance simulation
Kps = Ki*J/(B+Kd);
disp('Stability margin:');
disp(Kps);
Kp=3;
simOut = sim('model_pid_dist.mdl',paramNameValStruct);
u = simOut.find('u'); % control input vector
y = simOut.find('y'); % system output vector
t = simOut.find('t'); % time vector

f = figure('visible','off');
plot(t.Data,y.Data);grid;

saveas(f, 'model_pid_Kp_margin.pdf');

%[A,B,C,D]=linmod('model_pd_step');
%[numcl,dencl]=ss2tf(A,B,C,D);
%step(numcl,dencl, 1);
%step(numcl,dencl);

%% Feedforward simulation
simOut1 = sim('model_ff.mdl',paramNameValStruct);
uff = simOut1.find('u'); % control input vector
yff = simOut1.find('y'); % system output vector
tff = simOut1.find('t'); % time vector

simOut2 = sim('model_pid_new.mdl',paramNameValStruct);
upd = simOut2.find('u'); % control input vector
ypd = simOut2.find('y'); % system output vector
tpd = simOut2.find('t'); % time vector

f = figure('visible','off');
plot(tff.Data,yff.Data,tpd.Data,ypd.Data);grid;
xlabel('Time'); ylabel('Position'); title('System response'); 
legend('FF trajectory','PID trajectory');

saveas(f, 'model_ff_sr.pdf');

%% step
[A,BB,C,D]=linmod('model_ff');
[numcl,dencl]=ss2tf(A,BB,C,D);
step(numcl,dencl, 1);

